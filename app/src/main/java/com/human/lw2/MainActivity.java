package com.human.lw2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    ImageView imageView1;
    ImageView imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle("LW2AlectoApp");
        imageView  = (ImageView)findViewById(R.id.imageView);
        imageView1  = (ImageView)findViewById(R.id.imageView1);
        imageView2  = (ImageView)findViewById(R.id.imageView2);
        setDrawable();
    }
    private void setDrawable() {
        imageView.setImageResource(R.drawable.shape);
        imageView1.setImageResource(R.drawable.square);
        imageView2.setImageResource(R.drawable.oval);
    }
}
